package com.project.action;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.util.ReadAheadInputStream;

@WebServlet("/EditorUiAction")
public class EditorUiAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static String docRoot ;
       ExecuteProgram exp =new ExecuteProgram();
   
    public EditorUiAction() {
        super();
        
        // TODO Auto-generated constructor stub
    }

    public void init(ServletConfig config) {
    	docRoot = config.getServletContext().getRealPath("/");
        
   }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.print("servlet is calling");
		DataAccess dataobj=new DataAccess();
		 String action= request.getParameter("buttonclick");
		 String codetext= request.getParameter("sourcecode");
		 String savename = request.getParameter("prgm_name");
		 String username = request.getParameter("username");
		 
		 
		  if(action != null && action.isEmpty() != true){
		    	if("save".equals(action))
		    	{
		    		System.out.println(username);
		    		System.out.print("save button is clicked");
		    		
		    	dataobj.insertProgram(codetext, savename, username);
		    		System.out.println(savename);
		    		 request.setAttribute("sourcecode", codetext);
				        RequestDispatcher dispatcher = request.getRequestDispatcher("/onlineruneidefresh.jsp");
				        dispatcher.forward(request,  response);
			    	
		    		//dataobj.dataInsert(codetext);
		    		
		    	}
		    	else if("compile".equals(action))
		    	{
		    		System.out.print("compile button is clicked");
		    		System.out.print(codetext);
		    		//System.out.println(docRoot);
		    		
		    		
		    		String filename="test.run";
					File filep = new File(filename);
					if(!filep.exists()){
						filep.createNewFile();
					}
					
					
		    		
					FileWriter fw = new FileWriter(filep.getAbsoluteFile());
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(codetext);
					bw.close();
					System.out.println("content of file test.run");
					FileReader fr = new FileReader("test.run");
					BufferedReader br = new BufferedReader(fr);
					String prgm ;
					while((prgm = br.readLine())!=null)
					{
						System.out.println(prgm);
					}
					br.close();
					
					
					   String command = "runner test.run";

		                Process proc = Runtime.getRuntime().exec(command);

		                BufferedReader reader
		                        = new BufferedReader(new InputStreamReader(proc.getInputStream()));

		                String strContent = "";
		                String line = "";
		                while (( line = reader.readLine()) != null) {
		                	strContent = strContent + line; 
		                }
		                
		               // System.out.print("Content sent size"+ strContent);
		               
		    	
		    	
		    		
		    		//String strContent = exp.compileProgram(codetext);
	                System.out.println("Content sent size"+ strContent);
				    request.setAttribute("runeOutput", strContent);
				    request.setAttribute("sourcecode", codetext);
			        RequestDispatcher dispatcher = request.getRequestDispatcher("/onlineruneidefresh.jsp");
			        dispatcher.forward(request,  response);
		    	
		    	}
		    	else if("update".equals(action))
		    	{
		    		String prg_id = request.getParameter("prgm_id");
		    		System.out.println("program updating");
		    		System.out.println(prg_id);
		    		
		    		dataobj.updateProgram(prg_id, codetext);
		    		
		    		
		    	}
		    	else 
		    	{
		    		System.out.print("execute button is clicked");
		    		//Process proc = new ProcessBuilder("konsole").start();
		    		 Process proc1 = Runtime.getRuntime().exec("konsole");
		    		  BufferedReader reader = new BufferedReader(new InputStreamReader(proc1.getInputStream()));
		    		  System.out.println("run on terminal");
              String strContent = "";
              String line = "";
              while (( line = reader.readLine()) != null) {
              	System.out.println(line);
              }
              System.out.println(strContent);
		    	}
		    	
		  }
	}

	

}
