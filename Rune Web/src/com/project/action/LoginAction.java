package com.project.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LoginAction
 */
@WebServlet("/LoginAction")
public class LoginAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    
    public LoginAction() {
        super();
      
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean suc = false;
		DataAccess daobj= new DataAccess();
		String action= request.getParameter("submitaction");
		String name= request.getParameter("myname");
		String email = request.getParameter("mailid");
		String username= request.getParameter("username");
		String password = request.getParameter("password");
		String program = request.getParameter("program");
		//String signinaction = request.getParameter("signin");
		if(action != null && action.isEmpty() != true){
	    	if("create".equals(action))
	    	{
	    		daobj.createNewUser(name, email, username, password);
	    		System.out.print("create button is clicked");
	    		RequestDispatcher dispatcher = request.getRequestDispatcher("/login.jsp");
			     dispatcher.forward(request,  response);
	    		
	    	}
	    	if("login".equals(action))
			{
	    		System.out.println("sign in buton is clicked");
		    	 suc= daobj.existingUser(username, password);
		    		if(suc == true)
		    		{
		    			System.out.println("data in database");
		    		
		    			//redirect to the editor page using the requestDispatcher
		    			request.setAttribute("name", username);
		    			//request.setAttribute("user_id", user_id);
		    			 RequestDispatcher dispatcher = request.getRequestDispatcher("/prgmlist.jsp");
					     dispatcher.forward(request,  response);
					     
		    		}
		    		else
		    		{
		    			//redirect to the login error page.
		    			System.out.println("Not a registered user");
		    			 RequestDispatcher dispatcher = request.getRequestDispatcher("/loginerr.jsp");
					     dispatcher.forward(request,  response);
		    		}
		    }
	    	if("click here".equals(action))
	    	{
	    		System.out.println("button for list programs");
	    		String username1= request.getParameter("username");
	    		System.out.println(username1);
	    		
	    		List<Program> userProgramList =daobj.displayProg(username1);
	    		//System.out.println(programs);
	    		for(Program prg: userProgramList){
					System.out.println(prg.getProgramCode());
					System.out.println(prg.getProgramId());
					System.out.println(prg.getProgramName());
					String programName = prg.getProgramName();
	    	//	int prgid = daobj.getProgId(programs);
	    		//request.setAttribute("program", programs);
	    		//request.setAttribute("prgid", prgid);
	    		request.setAttribute("userProgramList", userProgramList);
	    		}
	    		//request.setAttribute("username", username);
   			 RequestDispatcher dispatcher = request.getRequestDispatcher("/prgmlist1.jsp");
			     dispatcher.forward(request,  response);
	    		
	    	
	    	}
		}
		
		
	    	
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String parameter= request.getParameter("param");
		DataAccess daobj= new DataAccess();
		System.out.println(parameter);
		//int prgid = 0 ;
		
	   String prgm = daobj.displayPid(parameter);
	   request.setAttribute("prgm_id", parameter);
	   request.setAttribute("prgm", prgm);
	   RequestDispatcher dispatcher = request.getRequestDispatcher("/onlineruneideopen.jsp");
	     dispatcher.forward(request,  response);
	}
	
}

