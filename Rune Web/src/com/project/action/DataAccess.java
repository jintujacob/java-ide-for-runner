package com.project.action;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;

import org.eclipse.jdt.internal.compiler.batch.Main;



public class DataAccess {
	
	public static Connection con = null;
	public static Statement stmt = null;
	public static Connection getConnection() throws ClassNotFoundException, SQLException
	{
		if(con==null)
		{
				Class.forName("com.mysql.jdbc.Driver");
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/rune", "root", "abc");
			    System.out.println("connection successful");
			    //stmt = con.createStatement();
			}
			    
		
				
		return con;
	}

/*	public void prgmInsert(int String code)
	{
		System.out.println("about to insert");
		
			
			String query="INSERT INTO DatabaseVer "
					+ "(prgm_id, username, program, output) "
					+ "VALUES (6, 'anjali','"+code+"' , 'no output')";
		/*	ResultSet rs= stmt.executeQuery("SELECT * FROM userdata");
			//stmt.execute("CREATE TABLE userdata(Id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, username VARCHAR(100), password VARCHAR(100))");
			//stmt.execute(query);
		//	boolean result = stmt.execute();
		/*	ResultSetMetaData rsmd = rs.getMetaData();
			int columnsNumber = rsmd.getColumnCount();   
			while(rs.next()){
				for(int i=1;i<columnsNumber;i++)
				System.out.println(rs.getString(i)+ " ");
			}*/
		
		
	public void createNewUser(String name, String mail, String username, String password)
	{
		
		Connection connection=null;
		try {
			Connection con = DataAccess.getConnection();
			String sql = "insert into UserInfo (username, password, mail_id, first_name) values (?,?,?,?)";
			PreparedStatement pstmt = null;
			ResultSet rs= null;
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, username);
			pstmt.setString(2, password);
			pstmt.setString(3, mail);
			pstmt.setString(4, name);
			pstmt.executeUpdate();
			List<User> userlist =new ArrayList<User>();
		//	rs = pstmt.executeUpdate();
		/*	while(rs.next()){
		
				User user=new User();
				user.setFirst_name(rs.getString("first_name"));
				user.setMail_id(rs.getString("mail_id"));
				user.setPassword(rs.getString("password"));
				user.setUsername(rs.getString("username"));
				/*System.out.println(rs.getString("username"));
				System.out.println(rs.getString("password"));
				System.out.println(rs.getString("mail_id"));
				System.out.println(rs.getString("first_name"));*/
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public boolean existingUser(String username, String password)
	{
		Connection connection=null;
		int count = 1;
		try {
			connection = DataAccess.getConnection();
			Statement stmt= connection.createStatement();
			String query="select count(*) as numrow from UserInfo where username = ? and password = ? ";
	PreparedStatement pstmt = null;
			
			pstmt = con.prepareStatement(query);
			pstmt.setString(1, username);
			pstmt.setString(2, password);
			ResultSet rs = pstmt.executeQuery();
			if(rs.next())
			{
				count = rs.getInt("numrow");
				System.out.println(rs.getString("numrow"));
				
			}
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(count==1)
		{
			System.out.println("SUCCESS");
			return true;
		}
		else
		{

			System.out.println("FAIL");
			return false;
		}
		

		
	}
	public List<Program> displayProg(String username)
	{
		String prgm= null;
		Connection connection=null;
		List<Program> userProgramList = new ArrayList<Program>();
		try {
			connection = DataAccess.getConnection();
			String sql = "select * from ProgramStore where username = ?";
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			pstmt = connection.prepareStatement(sql);
			pstmt.setString(1, username);
			
			
			rs = pstmt.executeQuery();
			
			while(rs.next())
			{
				Program prg = new Program();
				//System.out.println(rs.getString("program"));
				//System.out.println(rs.getString("prgm_id"));
				
				
				prg.setProgramId(rs.getString("prgm_id"));
				prg.setProgramName(rs.getString("prgm_name"));  // alter and add this colum in the program table as varchar
				prg.setProgramCode(rs.getString("prgm_code"));
				userProgramList.add(prg);
			    
			}
			
			// end of the loop. all items in the database are populated in the list
			// now return the userProgramList.
			
			
			// how to retrieve
			
			for(Program prg: userProgramList){
				System.out.println(prg.getProgramCode());
				System.out.println(prg.getProgramId());
				System.out.println(prg.getProgramName());
			}
		
			//return prgm;	
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return userProgramList;
		
		
	}
	public void updateProgram(String prg_id, String prgm_code)
	{
		int prgmid = 0;
		
		Connection connection=null;
		try {
			connection = DataAccess.getConnection();
			String sql = "update ProgramStore set prgm_code = ? where prgm_id =?";
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			pstmt = connection.prepareStatement(sql);
			pstmt.setString(1, prgm_code);
			pstmt.setString(2, prg_id);
			pstmt.executeUpdate();
		/*	while(rs.next())
			{
				 prgmid = rs.getInt("prgm_id");
			}
		
			//return prgm;	*/
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
		
	}
	
	public void insertProgram(String program, String prgm_name, String username)
	{
		ResultSet rs = null;
		Connection connection=null;
		try {
			
			Connection con = DataAccess.getConnection();
			String sql = "insert into ProgramStore(prgm_code, prgm_name, username) values (?, ?, ?)";
			PreparedStatement pstmt = null;
			
			pstmt = con.prepareStatement(sql);
					
			pstmt.setString(1, program);
			pstmt.setString(2, prgm_name);
			pstmt.setString(3, username);
			
			
			pstmt.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/*public int getUserid(String username ,String password)
	{
		String prgm = null;
		int user_id = 0;
		Connection connection=null;
		try {
			connection = DataAccess.getConnection();
			String sql = "select user_id from ProgramStore where username =? and password = ?";
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			pstmt = connection.prepareStatement(sql);
			pstmt.setString(1, username);
			pstmt.setString(2, password);
			
			
	
			
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				//System.out.println(rs.getString("program"));
				//System.out.println(rs.getString("prgm_id"));
			    user_id = rs.getInt("user_id");
			    System.out.println(prgm);
			    
			    
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user_id;
	
	/*public static void main(String[] args) throws ClassNotFoundException, SQLException {
		
			Connection con = DataAccess.getConnection();
			String sql = "select * from UserInfo where username=?";
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			pstmt = con.prepareStatement(sql);
			pstmt.setString(1, "arsha");
			rs = pstmt.executeQuery();
			
			while(rs.next()){
				System.out.println(rs.getString("username"));
				System.out.println(rs.getString("password"));
				System.out.println(rs.getString("mail_id"));
				System.out.println(rs.getString("first_name"));
			}
			
			*/
	
		public String displayPid(String prgm_id)
	{
		String prgm = null;
		int prg = 0;
		Connection connection=null;
		try {
			connection = DataAccess.getConnection();
			String sql = "select prgm_code from ProgramStore where prgm_id = ?";
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			pstmt = connection.prepareStatement(sql);
			pstmt.setString(1, prgm_id);
			
			
	
			
			rs = pstmt.executeQuery();
			while(rs.next())
			{
				//System.out.println(rs.getString("program"));
				//System.out.println(rs.getString("prgm_id"));
			    prgm = rs.getString("prgm_code");
			    System.out.println(prgm);
			    
			    
			}
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prgm;
	
	}
	
}
