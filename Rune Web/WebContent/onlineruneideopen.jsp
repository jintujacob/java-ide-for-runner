<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Rune Editor</title>
</head>
<%
String prgm_id = (String)request.getAttribute("prgm_id");


%>
<body>

<form action="EditorUiAction" method="get"> 
	<div id="container">
		<div id="content-header"><h1>RUNE EDITOR ONLINE...</h1></div>
		<p align= "right"><a href = "index.jsp">Back to home</a></p>
		<div id="content-menubar" 
		     style="border: 1px solid #ccc; width:900px;">
			<input type = "hidden" name = prgm_id value = <%=prgm_id %>>
			<input name= "buttonclick" type="submit" value="update"></input>
		
			<input name= "buttonclick" type="submit" value="compile"></input>
			<input name= "buttonclick" type="submit" value="execute"></input>
			
 		</div>
		
			<div style="float:left">
				<!-- line number -->
			</div>
			<div id="right-panel"
				 style = "float:left; min-height:400px; border:1px solid #ccc; width:890px; ">
				<textarea name="sourcecode" id="mytextarea"
						  style="height:390px; width:870px; margin-left:5px;"  >
						 
						  <%
				String codeTxt = "";
				if(null != request.getAttribute("prgm")){
					codeTxt =  (String)request.getAttribute("prgm");
				}%>
				
				<%=codeTxt %>	 
			  </textarea> 
			</div>
		</div> 
		<div id="content-output" 
 		     style="width:892px; border:1px solid #ccc; clear:both;">
			<textarea style="height:150px; width:880px; border:1px solid #ccc;">
			<%
				String codeText = "";
				if(null != request.getAttribute("runeOutput")){
					codeText =  (String)request.getAttribute("runeOutput");
				}%>
				
				<%=codeText %>	 
			</textarea>	
		</div>
	</div>
</form>


</body>
</html>