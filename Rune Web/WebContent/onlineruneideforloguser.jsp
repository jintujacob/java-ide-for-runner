<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import ="java.util.ArrayList" %>
    <%@page import ="com.project.action.Program" %>
    <%@page import ="com.project.action.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Rune Editor</title>

</head>

<%String username =(String) request.getParameter("param");%>
<%=username%>
<body>

<form action="EditorUiAction" method="get" name = "myform"> 
	<div id="container">
		<div id="content-header"><h1>RUNE EDITOR ONLINE...</h1></div>
		<p align= "right"><a href = "index.jsp">Back to home</a></p>
		<div id="content-menubar" 
		     style="border: 1px solid #ccc; width:900px;"  >
			<input type="text" name="prgm_name">
			<input type ="hidden" name ="username" value ="<%=username%>">
			<input name= "buttonclick" type="submit" value="save" ></input>
			<input name= "buttonclick" type="submit" value="compile"></input>
			<input name= "buttonclick" type="submit" value="execute"></input>
		</div>
		
			<div style="float:left">
				<!-- line number -->
			</div>
			<div>
			</div>
			
			
			<div id="right-panel"
				 style = "float:left; min-height:400px; border:1px solid #ccc; width:890px; ">
				<textarea name="sourcecode" id="mytextarea"
						  style="height:390px; width:870px; margin-left:5px;"  >
						  <%
				String codeTxt = "";
				if(null != request.getAttribute("sourcecode")){
					codeTxt =  (String)request.getAttribute("sourcecode");
				}
				%>
				
				<%=codeTxt %>	 
						  </textarea> 
			</div>
		</div> 
		<div id="content-output" 
 		     style="width:892px; border:1px solid #ccc; clear:both;">
			<textarea style="height:150px; width:880px; border:1px solid #ccc;">
			<%
				String codeText = "";
				if(null != request.getAttribute("runeOutput")){
					codeText =  (String)request.getAttribute("runeOutput");
				}%>
				
				<%=codeText %>	 
			</textarea>	
		</div>
	</div>

	
</form>

</body>
</html>