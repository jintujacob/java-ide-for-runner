<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>RUNE IDE</title>
<div id="header">
	<section>
	<body>

				<h1>RUNE COMPILER ONLINE</h1>
	</body>
	</section>


	<p align="right">
		<a href="index.jsp"><button>home</button></a> <a
			href="onlineruneide.jsp"><button>editor</button></a> <a
			href="login.jsp"><button>login</button></a>
			 <a href="help.jsp"><button>help</button></a>
	</p>
</div>
<head>
<style type="text/css">
#header {
	background-color: #696969;
	color: white;
	text-align: center;
	padding: 15px;
}

#nav {
	line-height: 30px;
	background-color: #eeeeee;
	height: 600px;
	width: 185px;
	float: left;
	padding: 5px;
}

#section {
	background-color: white;
  width:1050px;
	float: left;
	padding: 5px;
	color: black;
	text-align: justify;
}

#footer {
	background-color: #696969;
	color: white;
	clear: both;
	text-align: center;
	padding: 20px;
}
</style>

</head>
<div id="nav">
	<table>
				<tr>			
			<td color = "black"><a href="print.jsp">print</a></td>
		</tr>
		<tr>			
			<td><a href="read.jsp"> read</a></td>
		</tr>
		<tr>		
			<td><a href="array.jsp">array</a></td>
		</tr>
		<tr>			
			<td><a href="help.jsp">endl</a></td>
		</tr>
		<tr>			
			<td><a href="function.jsp">function</a></td>
		</tr>
		
		<tr>			
			<td><a href="string.jsp">Datatypes</a></td>
		</tr>
		
		<tr>			
			<td><a href="loop.jsp">Branching statements</a></td>
		</tr>
		<tr>			
			<td><a href="help.jsp">return</a></td>
		</tr>
		
		<tr>			
			<td><a href="branch.jsp">Looping statements</a></td>
		</tr>
		
		<tr>			
			<td><a href="help.jsp">exit</a></td>
		</tr>
	</table>

</div>
<div id="section" align="center">
	<body align = "center">
	<p><b>read: Reading Data</b></p>
       Syntax : read data

<p>Eg:</p>
<p>read a</p>
<p>It read a value to a</p>
<p>read x,y</p>
<p>It reads two values x and y.</p>
<b>readInt: Reading Integer</b>
      <p><b>readChar: Reading Character </b></p>
      
      <h5><b>readChar data</b></h5>
      <p>Eg:</p>
      <p>readChar a </p>
      <p>readChar ch1,ch2</p>
     <b>readString: Reading String</b>
     
      <p>Eg: readString a 
<p>Eg: readString ch1,ch2  
</p> 
	</body>
</div>
<div id="footer"></div>
</html>